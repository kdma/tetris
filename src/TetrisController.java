import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by kdma on 29/12/13.
 */
public class TetrisController {

    private static final int SPAWN_ROW = 0;
    private static final int SPAWN_COL = 4;
    private static final int ROWS = 20;
    private static final int COLUMNS = 10;
    private static final int MOVING = 1;
    private static final int EMPTY = 0;
    private int[][] _gameGrid;
    private int[][] _previewGrid;
    private TetrominoesFactory _tetroFactory ;
    private TetrominoesFactory.TetrominoType _currentTetro;
    private TetrisGui _gui;
    private TetrisController.KeyboardControl _keyboardController;
    private int _movingPieceRow = 0;
    private int _movingPieceCol = 0;
    private Timer _piecesGravity;

    private TetrisController(){
        _gameGrid = new int[ROWS][COLUMNS];
        _previewGrid = new int[4][4];
        _tetroFactory = new TetrominoesFactory();
        _keyboardController = new KeyboardControl();
        _gui = new TetrisGui(_gameGrid, _previewGrid);
        Init();
    }

    public TetrisGui GetGui(){
        return _gui;
    }

    private void Init(){
        InitBoardValues();
        _gui.AddKeyboardControllerListener(_keyboardController);
        ActionListener _taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                MoveDown();
            }

        };
        _piecesGravity = new Timer(1000, _taskPerformer);
        Play();
    }


    private void Play(){
        _currentTetro = SpawnPiece();
        DrawNextPiecePreview();
        System.out.print("spawned a" + _currentTetro.name() + "\n");
        if(IsPieceDrawable(SPAWN_ROW, SPAWN_COL)){
            DrawCurrentPiece(SPAWN_ROW, SPAWN_COL);
            _piecesGravity.start();
        }else{
            System.out.print("GAME OVER\n");
            _piecesGravity.stop();
       }
    }

    private void DrawNextPiecePreview(){
        TetrominoesFactory.TetrominoType preview = _tetroFactory.GetNextPiecePreview();
        for(int i = 0; i<4;i++){
            for(int j = 0;j<4;j++){
                _previewGrid[i][j] = preview._coords[i][j];
            }
        }
        _gui.UpdatePreview();
    }

    private void LandPiece(){
        for (int i = 0;i< ROWS;i++){
            for (int j = 0; j < COLUMNS; j++){
                if(_gameGrid[i][j] == MOVING){
                    _gameGrid[i][j] =_currentTetro.GetColorDescriptor();
                }
            }
        }
        _piecesGravity.stop();
        ClearFullLines();
        Play();
    }


    private void ClearFullLines(){
        for (int i = 0;i< ROWS;i++){
            int numberofCubesPerLine = 0;
            for (int j = 0; j < COLUMNS; j++){
                if(_gameGrid[i][j] != EMPTY ){
                    numberofCubesPerLine++;
                }
                if(numberofCubesPerLine == COLUMNS){
                    ClearLine(i);
                    MoveDownPiecesByOneRow(i);
                }
            }
        }
    }

    private void ClearLine(int line){
            for (int j = 0; j < COLUMNS; j++){
                _gameGrid[line][j] = EMPTY;
            }
        }

    private void MoveDownPiecesByOneRow(int indexOfLine){
        for (int i = indexOfLine ; i >= 0 ;i--){
            for(int j = 0 ; j < COLUMNS ; j++){
                    if(i >= 1){
                        _gameGrid[i][j] = _gameGrid[i-1][j];
                    }
        }
        _gui.UpdateBoard();

        }
    }

    private void InitBoardValues(){
        for(int i = 0; i < ROWS ; i++){
            for(int j = 0; j < COLUMNS; j++){
                _gameGrid[i][j] = EMPTY;
            }
        }
    }

    private TetrominoesFactory.TetrominoType SpawnPiece(){
        TetrominoesFactory.TetrominoType  piece = _tetroFactory.GetRandomPiece();
        _movingPieceRow = SPAWN_ROW;
        _movingPieceCol = SPAWN_COL;
        return piece;
    }

    private void DrawCurrentPiece(int x, int y){
            DrawPiece(x,y, _currentTetro, _gameGrid);
    }

    private void DrawPiece(int x, int y, TetrominoesFactory.TetrominoType tetro, int[][] grid){
        for(int i = tetro.GetStartingRowOfTetromino(); i < 4  ; i++, x++){
            for(int j = tetro.GetStartingColOfTetromino(); j < 4 ; j++){
                if(tetro._coords[i][j] != EMPTY){
                    System.out.print("Drawing a "+ _currentTetro.name() + " at "+ Integer.toString(x) +"," + Integer.toString(y+j) +"\n");
                    grid[x][y+j] = MOVING;
                    _gui.UpdateBoard(_currentTetro);
                }
            }
        }
    }

    private boolean IsPieceDrawable( int x, int y){
        int cubesThatCanBeDrawn = 0;
            for(int i = _currentTetro.GetStartingRowOfTetromino() ; i < 4  ; i++, x++){
                    for(int j = _currentTetro.GetStartingColOfTetromino(); j < 4 ;j++){
                        if(_currentTetro._coords[i][j] != EMPTY ){
                            if(IsInsideGridBox(x, y+j) && IsSpaceEmpty(x,y+j)){
                                cubesThatCanBeDrawn++;
                            }else{
                                //System.out.print(x + "," + y + " occupied\n");
                                return false;
                            }
                        }
                    }
            }

        return cubesThatCanBeDrawn == _currentTetro.GetNumberOfCubesOfPiece();
    }

    private boolean IsSpaceEmpty(int x, int y){
        int potentialPosition = _gameGrid[x][y];
        return potentialPosition == EMPTY || potentialPosition == MOVING;
    }

    private boolean IsInsideGridBox(int x, int y){
        return x < ROWS && x >= 0 && y < COLUMNS && y >= 0;
    }

    private void RemoveCurrentPiece( ){
        for(int i = 0; i < ROWS ; i++){
            for(int j = 0; j < COLUMNS; j++){
                if(_gameGrid[i][j] == MOVING){
                    _gameGrid[i][j] = EMPTY;
                }
             }
        _gui.UpdateBoard(_currentTetro);
        }
    }


    private void Rotate(){
        _currentTetro.RotatePiece();
        if(IsPieceDrawable(_movingPieceRow, _movingPieceCol)){
            System.out.print("moving " + _currentTetro.name() + "\n");
            AnimateMovingAction(_movingPieceRow, _movingPieceCol);
        }else{
            _currentTetro.RestorePreviousRotation();
        }
    }

    private void AnimateMovingAction(int pieceRow, int pieceCol){
        RemoveCurrentPiece();
        DrawCurrentPiece(pieceRow, pieceCol);
    }

    private void MoveLeft(){
        int potentialCol = _movingPieceCol - 1;
        if(IsPieceDrawable(_movingPieceRow, potentialCol) ){
            _movingPieceCol = potentialCol;
            AnimateMovingAction(_movingPieceRow, _movingPieceCol);
        }
    }

    private void MoveDown(){
        int potentialRow = _movingPieceRow + 1;
        if(IsPieceDrawable(potentialRow, _movingPieceCol)){
            _movingPieceRow = potentialRow;
            AnimateMovingAction(_movingPieceRow, _movingPieceCol);
        }else{
            LandPiece();
        }
    }

    private void MoveRight(){
        int potentialCol = _movingPieceCol + 1;
        if(IsPieceDrawable(_movingPieceRow, potentialCol)){
            _movingPieceCol = potentialCol;
            AnimateMovingAction(_movingPieceRow, _movingPieceCol);
        }
    }

    public JPanel GetOuterPanel(){
        return _gui.GetOuterContainer();
    }
    private class KeyboardControl implements KeyListener {

        @Override
        public void keyTyped(KeyEvent keyEvent) {
        }
        @Override
        public void keyReleased(KeyEvent keyEvent) {

            switch (keyEvent.getKeyCode()){
                case (KeyEvent.VK_LEFT): MoveLeft(); break;
                case (KeyEvent.VK_RIGHT): MoveRight(); break;
                case (KeyEvent.VK_DOWN): MoveDown(); break;
                case (KeyEvent.VK_UP): Rotate(); break;
            }
       
        }

        @Override
        public void keyPressed(KeyEvent keyEvent) {


        }
    }

    public static void main(String[] args) {

        TetrisController controller  = new TetrisController();
        JFrame frame = new JFrame("tetris");
        JLayeredPane lpane = new JLayeredPane();

        int frameHeigth = 760;
        int frameWidth =  550;
        int gameHeigth = 700;
        int gameWidth = 350;
        frame.setPreferredSize(new Dimension(frameWidth, frameHeigth));
        frame.setLayout(new BorderLayout());
        frame.add(lpane, BorderLayout.CENTER);
        lpane.setBounds(0, 0, frameWidth, frameHeigth);
        controller.GetOuterPanel().setBounds(0, 0, frameWidth, frameHeigth);
        controller.GetOuterPanel().setOpaque(true);
        controller.GetGui().GetGrid().setBounds(10, 10, gameWidth, gameHeigth);
        controller.GetGui().GetGrid().setOpaque(true);
        controller.GetGui().GetNextPieceGrid().setBounds(gameWidth+25, 10, 140, 140);
        controller.GetGui().GetGrid().setOpaque(true);

        lpane.add(controller.GetOuterPanel(), 0, 0);
        lpane.add(controller.GetGui().GetGrid(), 1, 0);
        lpane.add(controller.GetGui().GetNextPieceGrid(), 2, 0);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}
