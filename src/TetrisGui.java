import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;

/**
 * Created by kdma on 26/12/13.
 */
public class TetrisGui {

    private static final int ROWS = 20;
    private static final int COLUMNS = 10;
    private static final int EMPTY = 0;
    private JPanel _outerPanel;

    private int[][] _landedGrid;
    private int[][] _previewGrid;
    private JPanel _tetrisContainer;
    private JPanel[][] _tetrisBoard;
    private JPanel _previewContainer;
    private JPanel[][] _previewBoard;

    public TetrisGui(int[][] landed, int[][] nextPiece){
        _tetrisContainer = new JPanel();
        _tetrisBoard = new JPanel[ROWS][COLUMNS];
        _previewContainer = new JPanel();
        _previewBoard = new JPanel[4][4];
        _landedGrid = landed;
        _previewGrid = nextPiece;
        _outerPanel = new JPanel();
        Init();
    }

    private void Init(){
        CreateGameBoard();
        CreateNextPieceBoard();
    }

    public JPanel GetOuterContainer(){
        return _outerPanel;
    }

    private void CreateBoard(int rows, int columns, JPanel container, JPanel[][] grid){
        container.setLayout( new GridLayout( rows, columns));
        for (int i = 0; i < rows  ; i++){
            for(int j = 0; j < columns; j++){
                JPanel cube = new JPanel();
                cube.setBackground(Color.white);
                cube.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                grid[i][j] = cube;
                container.add(cube);
            }
        }
    }
    private void CreateNextPieceBoard(){
        CreateBoard(4,4, _previewContainer, _previewBoard);
    }

    private void CreateGameBoard(){
        CreateBoard(ROWS, COLUMNS, _tetrisContainer, _tetrisBoard);
        _tetrisContainer.setFocusable(true);
    }

    public void AddKeyboardControllerListener(KeyListener listener){
        _tetrisContainer.addKeyListener(listener);
    }


    public JPanel GetGrid(){
        return _tetrisContainer;
    }

    public JPanel GetNextPieceGrid(){
        return _previewContainer;
    }

    public void UpdatePreview(){
       PaintBoard(4,4, _previewGrid, _previewBoard, null);
    }

    public void UpdateBoard(TetrominoesFactory.TetrominoType tetromino){
        PaintBoard(ROWS, COLUMNS, _landedGrid, _tetrisBoard, tetromino);
    }

    private void PaintBoard(int rows, int cols, int[][] grid, JPanel[][] board, TetrominoesFactory.TetrominoType tetromino){
        for(int i = 0;i < rows; i++){
            for(int j = 0; j< cols; j++){
                if( grid[i][j] == EMPTY ){
                    board[i][j].setBackground(Color.white);
                }else{
                    switch (grid[i][j]) {
                        case 1:
                            board[i][j].setBackground(tetromino.GetColorOfPiece());
                            break;
                        case 2:
                            board[i][j].setBackground(Color.red);
                            break;
                        case 3:
                            board[i][j].setBackground(Color.CYAN);
                            break;
                        case 4:
                            board[i][j].setBackground(Color.pink);
                            break;
                        case 5:
                            board[i][j].setBackground(Color.green);
                            break;
                        case 6:
                            board[i][j].setBackground(Color.BLUE);
                            break;
                        case 7:
                            board[i][j].setBackground(Color.YELLOW);
                            break;
                        case 8:
                            board[i][j].setBackground(Color.MAGENTA);
                            break;
                    }
                }
            }
        }
    }

    public void UpdateBoard(){
       UpdateBoard(null);
    }


}
