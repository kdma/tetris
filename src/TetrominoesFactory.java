import java.awt.*;
import java.util.*;

/**
 * Created by kdma on 26/12/13.
 */
public class TetrominoesFactory {

    private TetrominoType _piece;
    private Queue<TetrominoType> _queue;
    private Color[] _colorMap;
    private static int ROWS = 4;
    private static int COLUMNS = 4;
    private int[][][][] TETRIS_PIECES;


    public TetrominoesFactory(){
        _queue = new LinkedList<TetrominoType>();
        Init();
    }

    private void Init(){
        _colorMap = new Color[]{
                Color.red,
                Color.CYAN,
                Color.pink,
                Color.green,
                Color.BLUE,
                Color.YELLOW,
                Color.MAGENTA
        };

        TETRIS_PIECES = new int[][][][] {
            new int[][][]
                     {
                           { { 0, 0, 0, 0 },  { 0, 0, 0, 0 },   { 2, 2, 0, 0 }, { 2, 2, 0, 0 } }, // Square block -> O 0°
                           { { 0, 0, 0, 0 },  { 0, 0, 0, 0 },   { 2, 2, 0, 0 }, { 2, 2, 0, 0 } }, // Square block -> O 90°
                           { { 0, 0, 0, 0 },  { 0, 0, 0, 0 },   { 2, 2, 0, 0 }, { 2, 2, 0, 0 } }, // Square block -> O 180°
                           { { 0, 0, 0, 0 },  { 0, 0, 0, 0 },   { 2, 2, 0, 0 }, { 2, 2, 0, 0 } }, // Square block -> O 270°
                    },
             new int[][][]
                    {
                            { { 3, 0, 0, 0 },  { 3, 0, 0, 0 },   { 3, 0, 0, 0 }, { 3, 0, 0, 0 } }, // Line Piece -> I  0°
                            { { 3, 3, 3, 3 },  { 0, 0, 0, 0 },   { 0, 0, 0, 0 }, { 0, 0, 0, 0 } }, // Line Piece -> I  90°
                            { { 3, 0, 0, 0 },  { 3, 0, 0, 0 },   { 3, 0, 0, 0 }, { 3, 0, 0, 0 } }, // Line Piece -> I 180°
                            { { 3, 3, 3, 3 },  { 0, 0, 0, 0 },   { 0, 0, 0, 0 }, { 0, 0, 0, 0 } }, // Line Piece -> I 270°
                    },
             new int[][][]
                    {
                            { { 0, 4, 0, 0 },  { 0, 4, 0, 0 },   { 0, 4, 4, 0 }, { 0, 0, 0, 0 } }, // L Block -> L   0°
                            { { 0, 0, 0, 0 },  { 0, 4, 4, 4 },   { 0, 4, 0, 0 }, { 0, 0, 0, 0 } }, // L Block -> L  90°
                            { { 0, 0, 0, 0 },  { 0, 4, 4, 0 },   { 0, 0, 4, 0 }, { 0, 0, 4, 0 } }, // L Block -> L  180°
                            { { 0, 0, 0, 0 },  { 0, 0, 4, 0 },   { 4, 4, 4, 0 }, { 0, 0, 0, 0 } }, // L Block -> L   270°
                    },
            new int[][][]
                    {
                            { { 0, 0, 0, 0 },  { 0, 0, 0, 0 },   { 0, 0, 5, 5 }, { 0, 5, 5, 0 } }, // Squiggly One -> S   0°
                            { { 0, 0, 0, 0 },  { 0, 5, 0, 0 },   { 0, 5, 5, 0 }, { 0, 0, 5, 0 } }, // Squiggly One -> S  90°
                            { { 0, 0, 0, 0 },  { 0, 0, 0, 0 },   { 0, 0, 5, 5 }, { 0, 5, 5, 0 } }, // Squiggly One -> S   0°
                            { { 0, 0, 0, 0 },  { 0, 5, 0, 0 },   { 0, 5, 5, 0 }, { 0, 0, 5, 0 } }, // Squiggly One -> S  90°
                    },
             new int[][][]
                    {
                            { { 0, 0, 0, 0 },  { 0, 0, 6, 0 },   { 0, 6, 6, 6 }, { 0, 0, 0, 0 } }, // T block -> T   0°
                            { { 0, 0, 0, 0 },  { 0, 0, 6, 0 },   { 0, 0, 6, 6 }, { 0, 0, 6, 0 } }, // T block -> T   90°
                            { { 0, 0, 0, 0 },  { 0, 0, 0, 0 },   { 0, 6, 6, 6 }, { 0, 0, 6, 0 } }, // T block -> T   0°
                            { { 0, 0, 0, 0 },  { 0, 0, 6, 0 },   { 0, 6, 6, 0 }, { 0, 0, 6, 0 } }, // T block -> T   90°
                    },
            new int[][][]
                    {
                            { { 0, 0, 0, 0 },  { 0, 0, 0, 0 },   { 7, 7, 0, 0 }, { 0, 7, 7, 0 } }, // Reverse Squiggly -> Z    0°
                            { { 0, 0, 0, 0 },  { 0, 7, 0, 0 },   { 7, 7, 0, 0 }, { 7, 0, 0, 0 } }, // Reverse Squiggly -> Z   90°
                            { { 0, 0, 0, 0 },  { 0, 0, 0, 0 },   { 7, 7, 0, 0 }, { 0, 7, 7, 0 } }, // Reverse Squiggly -> Z    0°
                            { { 0, 0, 0, 0 },  { 0, 7, 0, 0 },   { 7, 7, 0, 0 }, { 7, 0, 0, 0 } }, // Reverse Squiggly -> Z   90°
                    },
             new int[][][]
                    {
                            { { 0, 0, 8, 0 },  { 0, 0, 8, 0 },   { 0, 8, 8, 0 }, { 0, 0, 0, 0 } },  // Reverse L Block -> J  0°
                            { { 0, 0, 0, 0 },  { 0, 8, 0, 0 },   { 0, 8, 8, 8 }, { 0, 0, 0, 0 } },  // Reverse L Block -> J  90°
                            { { 0, 0, 0, 0 },  { 0, 8, 8, 0 },   { 0, 8, 0, 0 }, { 0, 8, 0, 0 } },  // Reverse L Block -> J  180°
                            { { 0, 0, 0, 0 },  { 8, 8, 8, 0 },   { 0, 0, 8, 0 }, { 0, 0, 0, 0 } },  // Reverse L Block -> J  270°
                    },
        };

        CreateQueue();

    }

    public TetrominoType GetRandomPiece(){
        _piece = _queue.poll();
        UpdateQueue();
        return _piece;
    }

    public TetrominoType GetNextPiecePreview(){
        return _queue.peek();
    }

    private void UpdateQueue(){
        _queue.add(setRandomShape());
    }

    private void CreateQueue(){
       _queue.add(setRandomShape());
       _queue.add(setRandomShape());
    }


    private TetrominoType setRandomShape()  {

        Random r = new Random();
        int randomPiece = Math.abs(r.nextInt()) %7;
        int randomRotation = Math.abs(r.nextInt()) %4;
        return new TetrominoType(randomPiece, randomRotation);
    }

    public class TetrominoType {


        private Color _color;
        public  int[][] _coords = new int[ROWS][COLUMNS];
        private int  _startingRowOfTetromino ;
        private int  _startingColOfTetromino ;
        private int NUMBERS_OF_CUBES_PER_PIECE = 4;
        private int _pieceIndex = 0;
        private int _rotationIndex = 0;


        private TetrominoType(int pieceIndex, int RotationIndex){
            _pieceIndex = pieceIndex;
            _rotationIndex = RotationIndex;
            _color = _colorMap[pieceIndex];
            _coords = TETRIS_PIECES[pieceIndex][RotationIndex];
            SetStartingRowOfTetromino();
            SetStartingColOffTetromino();
        }

        private void SetStartingRowOfTetromino(){
            for(int i = 0;i< ROWS;i++){
                for(int j = 0 ; j< COLUMNS; j++){
                    if(this._coords[i][j] != 0){
                         _startingRowOfTetromino = i;
                        return;
                    }
                }
            }
        }

        private void SetStartingColOffTetromino(){
            int leftMost = 5;
            for(int i = 0;i< ROWS;i++){
                for(int j = 0 ; j< COLUMNS; j++){
                    if(this._coords[i][j] != 0 && j < leftMost){
                        leftMost = j;
                    }
                }
            }
            _startingColOfTetromino = leftMost;
        }

        public Color GetColorOfPiece(){
            return _color;
        }

        public int GetStartingRowOfTetromino(){
            return _startingRowOfTetromino;
        }

        public int GetStartingColOfTetromino(){
            return _startingColOfTetromino;
        }
        public int GetNumberOfCubesOfPiece(){
            return NUMBERS_OF_CUBES_PER_PIECE;
        }

        public int GetColorDescriptor(){
            return _pieceIndex+2;
        }

        public String name(){
            switch (_pieceIndex){
                case 0: return "0";
                case 1: return "I";
                case 2: return "L";
                case 3: return "S";
                case 4: return "T";
                case 5: return "Z";
                case 6: return "J";
                default:return "ERROR";
            }
        }

        public void RestorePreviousRotation(){
            if(_rotationIndex == 0){
                _rotationIndex = 3;
            }else{
               _rotationIndex--;
            }
            _coords = TETRIS_PIECES[_pieceIndex][_rotationIndex];
            SetStartingRowOfTetromino();
            SetStartingColOffTetromino();
        }

        public void RotatePiece(){
           if(_rotationIndex == 3){
               _rotationIndex = 0;
           }else{
               _rotationIndex++;
           }
            _coords = TETRIS_PIECES[_pieceIndex][_rotationIndex];
            SetStartingRowOfTetromino();
            SetStartingColOffTetromino();
        }

    }




}
